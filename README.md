# PyDME

This is an extended and reorganized version of [`dolphin-memory-engine`](https://pypi.org/project/dolphin-memory-engine/). It has support for extended memory. JIT Cache updating and `dol` modification may be added in the future.

## To-do

- [ ] Extended memory support
- Useful classes to manage memory
    - [ ] Pointer array

## Functions

For now, import `pydme` and do `help(pydme)` to see the available functions.

## Credit

I've only put the parts together. Most credit goes to [Henrique](https://github.com/henriquegemignani) for making the original Python package that this is based on, and [Aldelaro](https://github.com/aldelaro5) for making the memory engine that *that* is based on. Additionally, [Minty-Meeo](https://github.com/Minty-Meeo) for patching `Dolphin-memory-engine` to work with extended memory.